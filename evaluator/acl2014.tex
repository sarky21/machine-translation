%
% File acl2014.tex
%
% Contact: koller@ling.uni-potsdam.de, yusuke@nii.ac.jp
%%
%% Based on the style files for ACL-2013, which were, in turn,
%% Based on the style files for ACL-2012, which were, in turn,
%% based on the style files for ACL-2011, which were, in turn, 
%% based on the style files for ACL-2010, which were, in turn, 
%% based on the style files for ACL-IJCNLP-2009, which were, in turn,
%% based on the style files for EACL-2009 and IJCNLP-2008...

%% Based on the style files for EACL 2006 by 
%%e.agirre@ehu.es or Sergi.Balari@uab.es
%% and that of ACL 08 by Joakim Nivre and Noah Smith

\documentclass[11pt]{article}
\usepackage{acl2014}
\usepackage{times}
\usepackage{url}
\usepackage{graphicx}
\usepackage{latexsym}

%\setlength\titlebox{5cm}

% You can expand the titlebox if you need extra space
% to show all the authors. Please do not make the titlebox
% smaller than 5cm (the original size); we will check this
% in the camera-ready version and ask you to change it back.


\title{Machine Translation HW-3 - Evaluation}

\author{Satya Prateek Bommaraju \\ sbommar1@jhu.edu}

\date{}

\begin{document}
\maketitle
\begin{abstract}
  The task of the homework assignment was to pick one of two system translations as best match with the reference translation. The metric we were graded on was accuracy of the predictions with respect to choice of a human evaluator. The baseline metric we were asked to implement was the simple meteor metric which gave an accuracy of 51\%. In addition to this I also implemented other evaluation metrics such as the actual Meteor metric and an a classifier approach for prediction. The performance of the meteor metric was 50.5\% on the dev data and the classifier when trained on 20\% of the dev data gave an accuracy of 55.5\%
  %%%% Write final output results
\end{abstract}

\section{Baseline implementation}

The baseline we were asked to implement for this assignment was the simple METEOR metric which is the weighted harmonic mean of precision and recall as given by
\[ l(h,e) = \frac{P(h,e).R(h,e)}{(1-\alpha)R(h,e) + \alpha P(h,e)} \]

This metric was very easy to implement. The only difficulty faced was with 0 values of precision and recall for some system translations which gave divide by 0 exceptions. To counter this I used smoothed values of counts for precision recall calculations. The best accuracy of 0.51 was obtained with an $\alpha$ value of 0.1 as can be seen from Figure 1

\begin{figure}[h] 
\caption{Accuracy results for various values of $\alpha$}
\centering
\includegraphics[scale=0.4]{meteor_one_alpha.png}
\end{figure}

\section{METEOR Metric}

\subsection{Implementation}
Since the results with simple meteor weren't that great, I decided to implement the actual METEOR metric proposed by Banerjee and Lavie as it was reported to have a high correlation with human judgements.

The METEOR metric was proposed to address several flaws or limitations of the BLEU metric that is commonly used for automatic evaluation. These include 
\begin{itemize}

\item
Does not take recall values into consideration

\item
Only considers word order via indirect measures such as higher order n-grams

\item
Does not use explicit word to word matching between the translation and reference.

\item
Wasn't designed for sentence level evaluation which is why taking geometric average of n-gram precisions can lead to 0 values

\end{itemize}

METEOR is calculated via several phases. In each phase a word alignment is created between words in the system translation vs words in the reference translation. They define a valid alignment as one in which each word in both the strings is matched to at-most one word in the other. The various alignment phases are exact phase (in which identical words are aligned), stemmer phase (where words are stemmed using the Porter Stemmer and then aligned) and finally the wordnet phase (where two words that are in each other's synsets as defined by wordnet are aligned).

There are other conditions to be noted for alignment. If there are more than one possible alignments in each phase the maximal valid alignment is picked and if there are more than one alignments with the maximum possible length then the alignment with the least "unigram crossing" is picked. Also after every phase only those words not yet aligned are considered for alignment. Thus the order of phases is very important. The ordering suggested by the authors is Exact, Stemming and Wordnet.

Then the authors calculate the F-measure using the same formula as the simple METEOR in section 1. They also implement a penalty to account for longer matches. This is done by calculating the fraction of chunks in a sentence (chunk is a consecutive sequence of words in the system translation that maps to a consecutive sequence of words in the reference translation.)
The penalty is calculated as 
\[ Penalty = \gamma*(\frac{\#chunks}{\#unigrams\_matched})^{\beta} \]
where $\gamma = 0.5$ and $\beta = 3$

The final score is calculated as 
\[ Score = Fmean*(1-Penalty) \]

\subsection{Results}
Disappointingly the performance of the METEOR metric was not very good, getting 0.49\% accuracy on the dev dataset using the default values of the parameters. I tried tuning the $\alpha, \beta, \gamma$ parameters manually but the runtime of the program was too lengthy (owing mainly to the greedy search for the maximal alignment) to make this feasible. However i found that in future paper the authors published tuned values of these parameters which they identified using a hill climb approach. For Czech-English and French-English the values they suggested was $\alpha = 0.9, \beta = 0.5, \gamma = 0.55$. 
These values did give better performance (50.5\%) but it was not as good an improvement as I had hoped.

Other improvements that have been suggested for the METEOR metric is the use of paraphrase tables in another alignment phase. However implementing this was not feasible for 2 reasons
1) The open source paraphrase extractor available online (parex) required a minimum of 12gb ram to run given pre-existing bilingual phrase tables and 2) The dataset we had to predict results for was not of just one language but from a cursoray glance included translations from Czech, French and Spanish. This rendered use of bilingual phrase tables in question because the paraphrases extracted may not be relevant.

\section{Classification Approach}

Since the METEOR metric did not give good enough results I decided to train a classifier to identify the best system translation. The reference paper was the one titled "\textbf{Regression and Ranking based Optimisation for Sentence Level Machine Translation Evaluation}" by Xingyi Song and Trevor Cohn.
Their idea was to train a ranking SVM that would learn to rank various candidate translations given a system translation. I decided to modify that idea a bit so as to fit with our current task.

\subsection{Implementation}

To fit the classification approach into our current task I had to modify the problem a little bit. Instead of outputting 1 when the first sentence was the best translation and -1 when the second was the best translation I modified it into a problem where the feature vectors for each input were the difference in values between the two system translations. Example the difference between meteor score of the first translation and the second or difference between clipped ngram precision. This is inspired by the method used to calculate the best translation using just the meteor score wherein the translation with the highest meteor score is picked. In the classifier approach we just let the algorithm pick the best option.

The features chosen for this task are:

\begin{itemize}

\item Difference in Meteor Score
\item Difference in n-gram precision for n = 1 to 4
\item Difference in n-gram clipped precision for n = 1 to 4
\item Difference in n-gram recall for n = 1 to 4
\item Difference in n-gram f-mesure for n = 1 to 4
\item Difference in Average n-gram precision
\item Difference in word count between system and reference
\item Difference in POS n-gram precision for n = 1 to 4
\item Difference in POS n-gram clipped precision for n = 1 to 4
\item Difference in POS n-gram recall for n = 1 to 4
\item Difference in POS n-gram f-mesure for n = 1 to 4

\end{itemize}

For classification I used the online learning library Vowpal Wabbit that is popular for giving fast and good results. The parameters for the algorithm were 500 passes over the training data (though it stopped after 12 passes as the log loss stopped improving) and the function to optimize was the logistic loss. Since this was a multiclass problem, the classification technique used was Error Correcting Tournament an approach which is supposed to give the best results for logistic loss.

\subsection{Results and Generalization - Avoiding Overfitting}

One of the problems I faced with this assignment was that we did not have any estimate for performance on the test dataset and we did not have a seperate development dataset. The first is reasonable but the absence of the second meant that while submitting our results we would have to predict on data already used for training. This has plenty of scope for overfitting and the results we get will be extremely skewed. For example predicting classes for the data with a model trained on the entire dataset gives an extremely high accuracy of 86\% which will clearly not generalize. 

To test how well the model will generalize I trained it on smaller and smaller subsets of the dataset and evaluated performance. Training on 50\% of the labelled data (10k samples) gives a classification accuracy of 65\% and training on a mere 20\% of the labelled data (5k samples) gives an accuracy of 55.5\% which is still much better than the METEOR score. The best generalization I assume however will come from the model trained on 50\% of the data. From figure 2 we can observe the best features used by the classifier.  From this we can see that the most discriminating features were high difference in word lengths, high differences in the METEOR score and differences in POS tag precisions. This is not surprising as word order and sentence length would be big differentiating factors between good and bad translations and all the above features take those into consideration.

\begin{figure}[h] 
\caption{Most informative features for training}
\centering
\includegraphics[scale=0.25]{classifier_informative_features.png}
\end{figure}


\section{Scope for improvement}
Further improvements could be made to this model by adding more features. One of the better performing evaulation techniques from the WMT10 contest was SVM-Rank method by University of Harbin. In that they achieve best results using a lot of metrics such as Bleu-Cum 1 to 5, Bleu-Ind 1 to 5, Letter Bleu metrics, all the rouge metrics (ROUGE-L, ROUGE-N, ROUGE-W, ROUGE-S, etc). Due to time constraints I couldn't implement these but if one could calculate all of these metrics and more, then given enough data the classifier could identify the most discriminative metrics.

\end{document}
