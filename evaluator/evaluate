#!/usr/bin/env python
from __future__ import division
import argparse # optparse is deprecated
from itertools import islice # slicing for iterators
from itertools import permutations
import sys
from nltk.stem import PorterStemmer, SnowballStemmer
from nltk.corpus import wordnet as wn
import math
 
def word_matches(h, ref):
    return sum(1 for w in h if w in ref)

def compute_precision(correct, out_len):
    return (correct)/(out_len)

def compute_recall(correct, ref_len):
    return (correct)/(ref_len)

def compute_f_mean(h_match, out_len, ref_len, alpha=0.9):
    p = compute_precision(h_match, out_len)
    r = compute_recall(h_match, ref_len)
    return p*r/((1-alpha)*r + alpha*p)

def compute_penalty(num_chunks, num_match, gamma = 0.55, beta = 0.5):
    """
    Computes the penalty of any alignment
    """
    return gamma * math.pow((num_chunks/num_match), beta)


def exact_map(h, ref):
    """
    This maps words if they exactly the same
    """
    alignment = []
    for word_pos, word in enumerate(h):
        for ref_pos, ref_word in enumerate(ref):
            if word == ref_word:
                alignment.append((word_pos, ref_pos)) #Add the alignment to the set            
    return alignment

def stemmer_map(h, ref, current_align):
    """
    Stems all words not aligned yet and tries to map them
    """
    stemmer = SnowballStemmer("english")
    def stem(word_list):
        #Generator to stem words handling unicodeDecode errors
        for word in word_list:
            stem_word = ""
            try:
                stem_word = stemmer.stem(word.decode("UTF-8"))
            except UnicodeDecodeError:
                yield word
            else:
                yield stem_word
    h_stemmed = stem(h)
    ref_stemmed = stem(h)
    alignment = []
    for word_pos, word in enumerate(h):
        for ref_pos, ref_word in enumerate(ref):
            if (word == ref_word) and ((word_pos, ref_pos) not in current_align):
                alignment.append((word_pos, ref_pos)) #Add the alignment to the set
    current_align.extend(alignment)           
    return current_align

def wordnet_map(h, ref, current_align):
    """
    Checks if two words are part of the same synset
    """
    alignment = []
    for word_pos, word in enumerate(h):
        for ref_pos, ref_word in enumerate(ref):
            #Check first if the words are not already aligned
            if (word_pos, ref_pos) not in current_align:
                #Check if the each word is in the synset of other
                def isWordInSynset(word, ref_word):
                    word_syn = []
                    try:
                        word_syn = wn.synsets(word.decode("UTF-8"))
                    except UnicodeDecodeError:
                        return False
                    if len(word_syn) == 0:
                        return False
                    for synset in word_syn:
                        lemma = synset.lemma_names()
                        if ref in lemma:
                            return True
                        else:
                            return False
                wordMatch = isWordInSynset(word, ref_word)
                refMatch = isWordInSynset(ref_word, word)
                if wordMatch and refMatch:
                     alignment.append((word_pos, ref_pos)) #Add the alignment to the set
    current_align.extend(alignment)
    return current_align

def best_mapping(alignment):
    """
    Compute the best mapping among all possibilities
    A greedy method that computes all options
    """
    alignment_list = []
    if alignment == []:
        return []
    for t_1 in alignment:
        align = [t_1]
        h_used = set()
        h_used.add(t_1[0])
        ref_used = set()  #Each word can be aligned atmost once
        h_used.add(t_1[1])
        add_align = align.append    
        for t_2 in alignment:
            if (t_2[0] not in h_used) and (t_2[1] not in ref_used):
                add_align(t_2)
                h_used.add(t_2[0])
                ref_used.add(t_2[1])
        alignment_list.append(align)

        #Try to find the alignment of maximum size
        max_alignment = max(map(lambda x: len(x), alignment_list))

        #Handle cases where the length is the same
        max_list = filter(lambda x: len(x) == max_alignment, alignment_list)
        num_max = len(max_list)

        if num_max > 0:
            #Select the set with least no. of unigram crosses
            minCross = float("inf")
            bestAlign = []
            for mapping in max_list:
                countCross = 0
                for (t_i, r_j), (t_k, r_l) in permutations(mapping, 2):
                    #Take all 2 pair mappings of bigrams
                    crossVal = (t_i - t_k)*(r_j - r_l)
                    if crossVal < 0:
                        countCross += 1
                if countCross < minCross:
                    minCross = countCross
                    bestAlign = mapping
            return bestAlign
        else:
            return max_list

def consume(iterator, n):
    """
    Advance the iterator n-steps ahead. If n is none, consume entirely
    """
    if n is None:
        # feed the entire iterator into a zero-length deque
        collections.deque(iterator, maxlen=0)
    else:
        # advance to the empty slice starting at position n
        next(islice(iterator, n, n), None)


def calculate_chunks(alignment):
    """
    Groups adjacent unigrams in system translation mapped to adjacent
    unigrams in the reference translation into chunks
    """
    num_chunks = 0
    chunk_list = []
    sequence = xrange(len(alignment)).__iter__()
    for i in sequence:
        chunk = []
        h_start, r_start = alignment[i]
        h_cur, r_cur = (h_start, r_start)
        #Loop forward until chunk ends
        if h_cur not in chunk: chunk.append(h_cur)
        for j in range(i+1, len(alignment)):
            h_adj, r_adj = alignment[j]
            if ((h_adj - h_cur) == 1) and ((r_adj - r_cur) == 1):
                if h_adj not in chunk: chunk.append(h_adj)
                h_cur, r_cur = (h_adj, r_adj)
                consume(sequence, 1)
            else:
                #Advance the iterator by j-i-1 steps
                break
        if len(chunk) > 0: chunk_list.append(chunk)
    return len(chunk_list)

def compute_meteor(h, ref):
    """
    Computes the meteor score of word and ref
    """
    exact_align = exact_map(h, ref)
    #Now find the best alignment
    best_exact = best_mapping(exact_align)
    #For words not aligned above align based on porter stemmer
    stemmer_align = stemmer_map(h, ref, best_exact)
    best_stem = best_mapping(stemmer_align)
    #For words not aligned above align based on wordnet
    wordnet_align = wordnet_map(h, ref, best_stem)
    best_wordnet = best_mapping(wordnet_align)

    num_match = len(best_wordnet)
    if num_match == 0:
        return 0 #Defining meteor score to be 0 when no match
    
    ### Finally compute the meteor score ####
    f_mean = compute_f_mean(num_match, len(h), len(ref))

    ### Calculate word match penalty. 
    num_chunks = calculate_chunks(best_wordnet)
    penalty = compute_penalty(num_chunks, num_match)

    return f_mean*(1 - penalty)

def main():
    parser = argparse.ArgumentParser(description='Evaluate translation hypotheses.')
    parser.add_argument('-i', '--input', default='data/hyp1-hyp2-ref',
            help='input file (default data/hyp1-hyp2-ref)')
    parser.add_argument('-n', '--num_sentences', default=None, type=int,
            help='Number of hypothesis pairs to evaluate')
    parser.add_argument('-a', '--alpha', default=0.2, type=float,
            help='Tuning parameter for Meteor metric')
    # note that if x == [1, 2, 3], then x[:None] == x[:] == x (copy); no need for sys.maxint
    opts = parser.parse_args()
 
    # we create a generator and avoid loading all sentences into a list
    def sentences():
        with open(opts.input) as f:
            for pair in f:
                yield [sentence.strip().split() for sentence in pair.split(' ||| ')]
 
    # note: the -n option does not work in the original code

    alpha = opts.alpha

    for h1, h2, ref in islice(sentences(), opts.num_sentences):
        rset = set(ref)

        #Build an alignment between the two sentences
        #Think of an alignment as a set of tuples (h1, h2)

        #Compute exact match alignment
        h1_meteor = compute_meteor(h1, rset)
        h2_meteor = compute_meteor(h2, rset)

        print(1 if h1_meteor > h2_meteor else # \begin{cases}
                (0 if h1_meteor == h2_meteor
                    else -1)) # \end{cases}
 
# convention to allow import of this file as a module
if __name__ == '__main__':
    main()
